let fs = require('fs');
let csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// Full method to sort csv files
const sortCsv = () => {
  // Initilize readstream
  let readstream = fs.createReadStream('./example.csv');

  // Set up arrays that will be used
  let companies = [];
  let newFiles = [];
  let sortedFiles = [];

  // Start the read stream
  readstream
    .pipe(csv())
    .on('data', chunk => {
      // Check to make sure data exists and doesn't error out
      if (chunk.userId) {
        // Check to see if company has been captured yet
        if (companies.indexOf(chunk.insuranceCompany) === -1) {
          // Add company to map
          companies.push(chunk.insuranceCompany);

          // Add first entry in newFiles for this company
          newFiles[companies.indexOf(chunk.insuranceCompany)] = [
            {
              userId: chunk.userId,
              name: chunk.name,
              sortName: reversName(chunk.name),
              version: chunk.version,
              insuranceCompany: chunk.insuranceCompany
            }
          ];
        } else {
          // Company already exists, pushing to that comapny
          newFiles[companies.indexOf(chunk.insuranceCompany)].push({
            userId: chunk.userId,
            name: chunk.name,
            sortName: reversName(chunk.name),
            version: chunk.version,
            insuranceCompany: chunk.insuranceCompany
          });
        }
      }
    })
    .on('end', () => {
      // Loop  to sort data in each company
      newFiles.forEach((collection, index) => {
        // First compare sortname, then size
        const tempCollection = collection.sort((a, b) =>
          a.sortName > b.sortName
            ? 1
            : a.sortName === b.sortName
            ? a.size > b.size
              ? 1
              : -1
            : -1
        );

        // Push data to clean array
        sortedFiles.push(tempCollection);
      });

      // Data is sorted, call method to write files
      writeFiles(sortedFiles);
    });
};

// Method to reverse first name and last name for sorting.
const reversName = name => {
  // Split the string at the space and reverse
  let reversed = name.split(' ').reverse();
  // Rebuild the string
  return `${reversed[0]}${reversed[1]}`;
};

// Method to write all the files
const writeFiles = files => {
  // Loop for each company's data
  files.forEach(file => {
    // Initialize file writer with insurance company as file name
    const csvWriter = createCsvWriter({
      path: `${file[0].insuranceCompany}.csv`,
      header: [
        { id: 'userId', title: 'User Id' },
        { id: 'name', title: 'Name' },
        { id: 'version', title: 'Version' },
        { id: 'insuranceCompany', title: 'Insurance Company' }
      ]
    });

    // Write the file, give confirmation.
    csvWriter
      .writeRecords(file)
      .then(() => console.log('The CSV file was written successfully'));
  });
};

module.exports = {
  sortCsv
};
